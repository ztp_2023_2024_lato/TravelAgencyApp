﻿using BiuroPodrozyAPI.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace BiuroPodrozyAPI.Services
{
    public interface ITravelAgencyService
    {
        int Create(CreateTravelAgencyDto dto);
        PagedResult<TravelAgencyDto> GetAll(TravelAgencyQuery query);
        public IEnumerable<TravelAgencyDto> GetMyAgency();

        TravelAgencyDto GetById(int id);
        void Delete(int id);
        void Update(int id, UpdateTravelAgencyDto dto);
        IEnumerable<TravelAgencyDto> GetTravelAgencyByCity(string city);
    }
}