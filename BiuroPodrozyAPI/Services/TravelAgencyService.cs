﻿using AutoMapper;
using BiuroPodrozyAPI.Authorization;
using BiuroPodrozyAPI.Entitties;
using BiuroPodrozyAPI.Exceptions;
using BiuroPodrozyAPI.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;

namespace BiuroPodrozyAPI.Services
{
    public class TravelAgencyService : ITravelAgencyService
    {
        private readonly TravelAgencyDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<TravelAgencyService> _logger;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUserContextService _userContextService;

        public TravelAgencyService(TravelAgencyDbContext dbContext, IMapper mapper, ILogger<TravelAgencyService> logger
            , IAuthorizationService authenticationService, IUserContextService userContextService)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _authorizationService = authenticationService;
            _userContextService = userContextService;
        }

        public void Delete(int id)
        {
            var travelAgencyById = _dbContext
               .TravelAgencies
               .FirstOrDefault(x => x.Id == id);

            if(travelAgencyById is null)
            {
                throw new NotFoundException("Travel agency not found");
            }

            var authorizationResult = _authorizationService.AuthorizeAsync(_userContextService.User, travelAgencyById,
                new ResourceOperationRequirement(ResourceOperation.Delete)).Result;

            if (!authorizationResult.Succeeded)
            {
                throw new ForbidException();
            }

            _dbContext.TravelAgencies.Remove(travelAgencyById);
            _dbContext.SaveChanges();
        }

        public IEnumerable<TravelAgencyDto> GetMyAgency()
        {
            // Pobierz identyfikator aktualnie zalogowanego użytkownika
            var userId = _userContextService.GetUserId;

            var myTravelAgencies = _dbContext
               .TravelAgencies
               .Include(t => t.Address)
               .Include(t => t.Offers)
               .Where(t => t.CreatedById == userId)
               .ToList();

            var travelAgencyDtos = _mapper.Map<List<TravelAgencyDto>>(myTravelAgencies);

            return travelAgencyDtos;
        }



        public TravelAgencyDto GetById(int id)
        {
            var travelAgencyById = _dbContext
               .TravelAgencies
               .Include(t => t.Address)
               .Include(t => t.Offers)
               .FirstOrDefault(x => x.Id == id);

            if (travelAgencyById is null)
            {
                throw new NotFoundException("Travel agency not found");
            }

            var travelAgencyDto = _mapper.Map<TravelAgencyDto>(travelAgencyById);
            return travelAgencyDto;
        }

        public IEnumerable<TravelAgencyDto> GetTravelAgencyByCity(string city)
        {
            var travelAgency = _dbContext
               .TravelAgencies
               .Include(t => t.Address)
               .Include(t => t.Offers)
               .Where(a => a.Address.City == city)
               .ToList();

            if(travelAgency is null)
            {
                throw new NotFoundException("Travel agency not found");
            }

            var travelAgencyDtos = _mapper.Map<List<TravelAgencyDto>>(travelAgency);
            return travelAgencyDtos;
        }

        public PagedResult<TravelAgencyDto> GetAll(TravelAgencyQuery query)
        {
            var baseQuery = _dbContext
               .TravelAgencies
               .Include(t => t.Address)
               .Include(t => t.Offers)
               .Where(t => query.SearchPhrase == null || (t.Name.ToLower().Contains(query.SearchPhrase.ToLower())
               || t.Description.ToLower().Contains(query.SearchPhrase.ToLower())));

            if(!string.IsNullOrEmpty(query.SortBy))
            {
                var columnsSelector = new Dictionary<string, Expression<Func<TravelAgency, object>>>
                {
                    { nameof(TravelAgency.Name), t => t.Name },
                    { nameof(TravelAgency.Description), t => t.Description },
                    { nameof(TravelAgency.Address.City), t => t.Address.City },
                };

                var selectedColumn = columnsSelector[query.SortBy];

                baseQuery = query.SortDirection == SortDirection.ASC 
                    ? baseQuery.OrderBy(selectedColumn)
                    : baseQuery.OrderByDescending(selectedColumn);
            }

            var travelAgency = baseQuery
               .Skip(query.PageSize * (query.PageNumber - 1))
               .Take(query.PageSize)
               .ToList();

            var totalItemsCount = baseQuery.Count();

            var travelAgencyDtos = _mapper.Map<List<TravelAgencyDto>>(travelAgency);

            var result = new PagedResult<TravelAgencyDto>(travelAgencyDtos, totalItemsCount, query.PageSize, query.PageNumber);

            return result;
        }

        public int Create(CreateTravelAgencyDto dto)
        {
            var travelAgency = _mapper.Map<TravelAgency>(dto);
            travelAgency.CreatedById = _userContextService.GetUserId;
            _dbContext.TravelAgencies.Add(travelAgency);
            _dbContext.SaveChanges();

            return travelAgency.Id;
        }

        public void Update(int id, UpdateTravelAgencyDto dto)
        {
            var travelAgency = _dbContext
              .TravelAgencies
              .FirstOrDefault(x => x.Id == id);

            if (travelAgency is null)
                throw new NotFoundException("Travel agency not found");


            var authorizationResult = _authorizationService.AuthorizeAsync(_userContextService.User, travelAgency, 
                new ResourceOperationRequirement(ResourceOperation.Update)).Result;

            if(!authorizationResult.Succeeded)
            {
                throw new ForbidException();
            }

            travelAgency.Name = dto.Name;
            travelAgency.Description = dto.Description;
            travelAgency.ContactEmail = dto.ContactEmail;
            travelAgency.ContactNumber = dto.ContactNumber;

            _dbContext.SaveChanges();
        }
    }
}
