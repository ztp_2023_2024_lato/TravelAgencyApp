﻿using AutoMapper;
using BiuroPodrozyAPI.Entitties;
using BiuroPodrozyAPI.Exceptions;
using BiuroPodrozyAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BiuroPodrozyAPI.Services
{
    public interface IAllOffersService
    {
        List<OfferDto> GetAllOffers();
    }


    public class AllOffersService : IAllOffersService
    {
        private readonly TravelAgencyDbContext _context;
        private readonly IMapper _mapper;
        public AllOffersService(TravelAgencyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public List<OfferDto> GetAllOffers()
        {
            var offers = _context.Offers.ToList();
            if (offers == null || offers.Count == 0)
            {
                throw new NotFoundException("Offers not found");
            }

            var offerDtos = _mapper.Map<List<OfferDto>>(offers);
            return offerDtos;
        }
    }
}
