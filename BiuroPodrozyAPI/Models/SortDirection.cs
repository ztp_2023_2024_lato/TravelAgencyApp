﻿namespace BiuroPodrozyAPI.Models
{
    public enum SortDirection
    {
        ASC,
        DESC
    }
}
