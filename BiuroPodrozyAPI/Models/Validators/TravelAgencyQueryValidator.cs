﻿using BiuroPodrozyAPI.Entitties;
using FluentValidation;
using System.Linq;

namespace BiuroPodrozyAPI.Models.Validators
{
    public class TravelAgencyQueryValidator : AbstractValidator<TravelAgencyQuery>
    {
        private int[] allowedPageSizes = new[] { 5, 10, 15 };
        private string[] allowedSordByColumnNames = { nameof(TravelAgency.Name), nameof(TravelAgency.Description), nameof(TravelAgency.Address.City) };
        public TravelAgencyQueryValidator()
        {
            RuleFor(r => r.PageNumber).GreaterThanOrEqualTo(1);
            RuleFor(r => r.PageSize).Custom((value, context) =>
            {
                if(!allowedPageSizes.Contains(value))
                {
                    context.AddFailure("PageSize", $"PageSize must in [{string.Join(",", allowedPageSizes)}]");
                }
            });

            RuleFor(r => r.SortBy)
                .Must(value => string.IsNullOrEmpty(value) || allowedSordByColumnNames.Contains(value))
                .WithMessage($"Sort by is optional, or must be in [{string.Join(",", allowedSordByColumnNames)}]");

        }
    }
}
