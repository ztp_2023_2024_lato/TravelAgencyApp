﻿using BiuroPodrozyAPI.Models;
using BiuroPodrozyAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BiuroPodrozyAPI.Controllers
{
    [Route("travelagency/alloffers")]
    [ApiController]
    public class AllOffersController : ControllerBase
    {
        private readonly IAllOffersService _allOffersService;

        public AllOffersController(IAllOffersService allOffersService)
        {
            _allOffersService = allOffersService;
        }

        [HttpGet]
        public ActionResult<List<OfferDto>> GetAllOffers()
        {
            var result = _allOffersService.GetAllOffers();
            return Ok(result);
        }
    }
}

